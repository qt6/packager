#include "lineeditwidget.h"

#include <QDebug>
//#include <QApplication>
#include <QHBoxLayout>

#include <QMimeData>
#include <QDragEnterEvent>

#include <QPushButton>
#include <QFileDialog>
#include <QFileInfo>
#include <QFile>
#include <QDir>

DropLineEdit::DropLineEdit(QWidget *parent)
    :QLineEdit(parent)
{
    setAcceptDrops(true);
}

void DropLineEdit::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("text/plain"))
        event->acceptProposedAction();
}

void DropLineEdit::dropEvent(QDropEvent *event)
{

    QUrl url(event->mimeData()->text());

    setText(url.toLocalFile().simplified());

    event->acceptProposedAction();
}

LineEditWidget::LineEditWidget(TypeFile typeFile, QWidget *parent)
    :QWidget(parent),
      m_typeFile(typeFile)
{
    m_lineEdit = new DropLineEdit(this);
    m_btn = new QPushButton("...", this);


    auto mainLay = new QHBoxLayout(this);


    mainLay->addWidget(m_lineEdit);
    mainLay->addWidget(m_btn);


    connect(m_btn, &QPushButton::clicked,[this]{


        QString path;
        QString curDir;


        QFileInfo curText(m_lineEdit->text().simplified());


        curDir = curText.absoluteFilePath();

        if(curText.isFile())
            curDir = curText.absolutePath();

        // TODO: add support symlink

//        if(curText.isSymLink())
//            curDir = curText.absolutePath();

        switch (m_typeFile) {

        case Dir:
            path = QFileDialog::getExistingDirectory(this, "Select Directory", curDir);
            break;
        case File:
            path = QFileDialog::getOpenFileName(this, "Select File", curDir,"All files(*);;Config (*.json);;Script (*.iss);;Archive (*.zip)");
            break;
        default:
            break;
        }


        if(path.isEmpty())
            return;


        m_lineEdit->setText(path);

    });
}

LineEditWidget::LineEditWidget(LineEditWidget::TypeFile typeFile, const QString &placeholderText, QWidget *parent)
    :LineEditWidget(typeFile, parent)
{
    m_lineEdit->setPlaceholderText(placeholderText);
}

void LineEditWidget::setText(const QString &text)
{
    m_lineEdit->setText(text);
}
