#include <QApplication>

#include "widget.h"

#include "optionsaver.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    OptionSaver saver;

    Widget w;
    w.show();
    return a.exec();
}
