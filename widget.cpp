#include "widget.h"


#include <QDebug>
#include <QDir>


#include <QFormLayout>
#include <QPushButton>

#include "qzipreader_p.h"
#include "qzipwriter_p.h"

#include "lineeditwidget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{

    setMinimumSize(640, 480);

    // TODO: add checkbox to fullrewrite and rename in collision


    auto mainLay = new QFormLayout(this);

    auto compilerLE = new LineEditWidget(LineEditWidget::File, "Compiler exe", this);
    auto sourceDirLE = new LineEditWidget(LineEditWidget::Dir, this);
    auto buildDirLE = new LineEditWidget(LineEditWidget::Dir, this);

    auto prevDirLE = new LineEditWidget(LineEditWidget::Dir,"Prev dir from copy", this);
    auto arcSrcBtn = new QPushButton("Archive src", this);
    auto copyBtn = new QPushButton("Copy", this);
    //    auto sourceDirLE = new LineEditWidget(LineEditWidget::Dir,"Source project dir", this);
    auto versionrLE = new QLineEdit(this);
    auto zipLE = new LineEditWidget(LineEditWidget::File, "Archive file, *.zip", this);
    auto unzipBtn = new QPushButton("Unzip", this);
    auto buildTargetLE = new LineEditWidget(LineEditWidget::Dir, this);
    auto buildTargetBtn = new QPushButton("Copy build ", this);
    //    auto targetDirLE = new LineEditWidget(LineEditWidget::Dir,"Target dir to copy", this);
    auto scriptLE = new LineEditWidget(LineEditWidget::File, "Script file, *.iss", this);
    auto compileBtn = new QPushButton("Compile", this);






    mainLay->addRow("Compiler file", compilerLE);
    mainLay->addRow("Source dir", sourceDirLE);
    mainLay->addRow("Build dir", buildDirLE);
    mainLay->addRow("Prev path", prevDirLE);
    mainLay->addRow("Version", versionrLE);
    mainLay->addRow(copyBtn);
    mainLay->addRow(arcSrcBtn);
    mainLay->addRow("Archive file", zipLE);
    mainLay->addRow(unzipBtn);
    mainLay->addRow("Build target", buildTargetLE);
    mainLay->addRow(buildTargetBtn);
    mainLay->addRow("Script file", scriptLE);
    mainLay->addRow(compileBtn);

    //    mainLay->addRow("Target path", targetDirLE);
    //    mainLay->addRow("Script file", scriptLE);
    //    mainLay->addRow("Compiler file", compilerLE);
    //    mainLay->addRow("Archive file", zipLE);
    //


    connect(copyBtn, &QPushButton::clicked, [this, prevDirLE, versionrLE, zipLE, scriptLE, buildTargetLE]{

        QFileInfo srcInfo(prevDirLE->text());

        if(!srcInfo.isDir())
            return;

        QString workDirPath = srcInfo.absolutePath() + QDir::separator() + versionrLE->text();

        if(QDir(workDirPath).exists())
            QDir(workDirPath).removeRecursively();

        copyPath(prevDirLE->text(), workDirPath);

        sourceDirPath = workDirPath + QDir::separator() + "source";

        QDir setupDir(setupDirPath = workDirPath + QDir::separator() + "setup");

        const auto zipes = setupDir.entryList(QStringList()<<"*.zip", QDir::Files);

        if(zipes.size())
            zipLE->setText(setupDirPath + QDir::separator() + zipes.at(0));

        scriptLE->setText(setupDirPath);
        buildTargetLE->setText(setupDirPath);

    });

    connect(arcSrcBtn, &QPushButton::clicked, [this]{


        const QString &zipName = sourceDirPath + QDir::separator() + "source.zip";


        QZipWriter writer(zipName);






    });

    connect(unzipBtn, &QPushButton::clicked, [zipLE, buildTargetLE]{


        QFileInfo arcInfo(zipLE->text());


        if(!arcInfo.isFile())
            return;



        QZipReader reader(zipLE->text());


        reader.extractAll(arcInfo.absolutePath());


        QFile::remove(arcInfo.absoluteFilePath());

        buildTargetLE->setText(arcInfo.absolutePath());

    });

    connect(buildTargetBtn, &QPushButton::clicked, [this, buildTargetLE, buildDirLE]{
        const QString &srcPath = buildDirLE->text();
        const QString &dstPath = buildTargetLE->text();

        QDir dstDir(dstPath);

        // TODO: add selection
        if(dstDir.exists())
            dstDir.removeRecursively();

        dstDir.mkpath(dstPath);

        copyPath(srcPath, dstPath);


    });

    connect(compileBtn, &QPushButton::clicked, [this]{

    });


    getData();

}

Widget::~Widget()
{
}

void Widget::copyPath(const QString &src, const QString &dst) const
{
    QDir dir(src);
    if (! dir.exists())
        return;
    for (const QString &d : dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        const QString &dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path);
    }

    for (const QString &f : dir.entryList(QDir::Files)) {

        const QString &newFileName = dst + QDir::separator() + f;

        if(QFile::exists(newFileName))
            QFile::remove(newFileName);

        QFile::copy(src + QDir::separator() + f, newFileName);
    }
}

void Widget::setData()
{

}

void Widget::getData()
{

}
