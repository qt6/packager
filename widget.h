#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class Widget : public QWidget
{
    Q_OBJECT

    void copyPath(const QString &src, const QString &dst) const;

    void setData();
    void getData();


    QString setupDirPath;
    QString sourceDirPath;

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
};
#endif // WIDGET_H
