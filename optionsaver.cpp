#include "optionsaver.h"

#include <QApplication>

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

QString Path::get(const QString &key)
{
    return qApp->applicationDirPath() + '/' + key;
}

bool JsonSaver::save(const QJsonObject &json, const QString &fileName)
{
    QFile saveFile(fileName);
    if (!saveFile.open(QIODevice::WriteOnly))
        return false;

    QJsonDocument document(json);
    saveFile.write(document.toJson());
    saveFile.close();

    return true;
}

bool JsonSaver::load(QJsonObject &json, const QString &fileName)
{
    QFile loadFile(fileName);
    if (!loadFile.open(QIODevice::ReadOnly))
        return false;

    QJsonDocument document(QJsonDocument::fromJson(loadFile.readAll()));
    json = document.object();
    loadFile.close();

    return true;
}

OptionSaver::OptionSaver()
{
    QJsonObject json;
    if (Path::get(Path::options).contains(".json") && JsonSaver::load(json, Path::get(Path::options)))
    {
        AppOptions::global().load(json["AppOptions"].toObject());
    }
}

OptionSaver::~OptionSaver()
{
    if (Path::get(Path::options).contains(".json"))
    {
        QJsonObject json;

        json["AppOptions"] = AppOptions::global().save();

        JsonSaver::save(json, Path::get(Path::options));
    }
}

void AppOptions::reset()
{

}

QJsonObject AppOptions::save() const
{
    QJsonObject json;


    return json;
}

void AppOptions::load(const QJsonObject &json)
{

}

void GeneralOptions::reset()
{
    appOptions.clear();
}

QJsonObject GeneralOptions::save() const
{
    QJsonObject json;
    QJsonArray appOptionsArr;


    for(const auto &appOpt : appOptions){
        appOptionsArr.push_back(appOpt.save());
    }


//    json["lastOptions"] = lastOptions.save();

    json["appOptions"] = appOptionsArr;
    return json;
}

void GeneralOptions::load(const QJsonObject &json)
{

}
