#ifndef LINEEDITWIDGET_H
#define LINEEDITWIDGET_H

#include <QLineEdit>

class DropLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit DropLineEdit(QWidget *parent = nullptr);
protected:
    virtual void dragEnterEvent(QDragEnterEvent *event) override;
    virtual void dropEvent(QDropEvent *event) override;
};

class QPushButton;

class LineEditWidget : public QWidget
{
    Q_OBJECT

public:

    enum TypeFile{
        Unknow,
        Dir,
        File
    };

    explicit LineEditWidget(TypeFile typeFile,QWidget *parent = nullptr);
    explicit LineEditWidget(TypeFile typeFile,const QString &placeHolderText, QWidget *parent = nullptr);


    void setText(const QString &text);

    QString text() const{
        return m_lineEdit->text().simplified();
    }


private:

    DropLineEdit *m_lineEdit;
    QPushButton *m_btn;


    TypeFile m_typeFile;

};

#endif // LINEEDITWIDGET_H
