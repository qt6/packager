#ifndef OPTIONSAVER_H
#define OPTIONSAVER_H

#include <QString>
#include <QList>

class QJsonObject;

namespace Path {

    QString get(const QString &key);

    const QString options {"setup/options.json"};

};

class JsonSaver
{
public:
    static bool save(const QJsonObject &json, const QString &fileName);
    static bool load(QJsonObject &json, const QString &fileName);
};

class OptionSaver
{
public:
    OptionSaver();
    ~OptionSaver();
};


class AbstractOptions
{

public:

    virtual void reset() = 0;

    virtual QJsonObject save() const = 0;
    virtual void load(const QJsonObject &json) = 0;

};

class AppOptions : public AbstractOptions
{
public:

    static AppOptions &global(){
        static AppOptions opt;
        return opt;
    }

    virtual void reset() override;

    virtual QJsonObject save() const override;
    virtual void load(const QJsonObject &json) override;

private:

    AppOptions(){
        reset();
    }

};

class GeneralOptions : public AbstractOptions
{
public:

    static GeneralOptions &global(){
        static GeneralOptions opt;
        return opt;
    }

    virtual void reset() override;

    virtual QJsonObject save() const override;
    virtual void load(const QJsonObject &json) override;


    QList <AppOptions> appOptions;
//    AppOptions lastOptions;

private:

    GeneralOptions(){
        reset();
    }



};

#endif // OPTIONSAVER_H
